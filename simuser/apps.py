from django.apps import AppConfig


class SimuserConfig(AppConfig):
    name = 'simuser'
