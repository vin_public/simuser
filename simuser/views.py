from django.conf import settings
from django.forms.models import modelform_factory
from django.views.generic import View
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView, UpdateView
from django.contrib.auth.models import User
from simuser.models import Profile, Friendship
from django.core.urlresolvers import reverse, reverse_lazy
from django import forms
from django.views.generic import TemplateView
from django.views.generic.detail import SingleObjectMixin

# Forms

class FriendshipForm(forms.ModelForm):
    class Meta:
        model = Friendship
        fields = [ 'confirmed' ]
        widgets = { 'confirmed': forms.RadioSelect }

    def get_confirmed(self):
        return self.cleaned_data['confirmed']

class UserForm(forms.ModelForm):

    class Meta:
        model = User
        widgets = {
            'password': forms.PasswordInput(),
        }
        fields = [ 'username', 'password' ]

    def createuser(self):
        user = User.objects.create_user(self.cleaned_data['username'])
        user.set_password(self.cleaned_data['password'])
        user.save()

# Our views

class RegistrationView(FormView):
    form_class = UserForm
    template_name = 'simuser/user_create_form.html'
    success_url = '/accounts/login/'

    def form_valid(self, form):
        form.createuser()
        return super(RegistrationView, self).form_valid(form)

class UsersListView(ListView):
    model = User
    context_object_name = 'users'
    template_name = 'simuser/users_list.html'

class ProfileFormView(View):
    def get(self, request, *args, **kwargs):
        view = ProfileView.as_view()
        return view(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return super(ProfileFormView, self).post(request, *args, **kwargs)
        view = FriendshipView.as_view()
        return view(request, *args, **kwargs)

class ProfileView(DetailView):
    model = Profile
    context_object_name = 'profile'
    template_name ='simuser/show_user_profile.html'
    fields = [ 'birthday', 'bio' ]

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            if self.request.user != self.object.user:
                mutuals_count = 0
                form = None
                friendship = None
                try:
                    friendship = Friendship.objects.get(profile=self.request.user.profile, friend=self.object.user)
                except Friendship.DoesNotExist:
                    form = FriendshipForm({'profile': self.request.user.profile, 'friend': self.object.user})
                else:
                    form = FriendshipForm(instance=friendship)
                finally:
                    context['form'] = form
                try:
                    reversed_friendship = Friendship.objects.get(profile=self.object, friend=self.request.user)
                except Friendship.DoesNotExist:
                    pass
                else:
                    if reversed_friendship.confirmed:
                        if not friendship: context['message'] = 'This person has sent you a friend request.'
                    else:
                        context['message'] = 'This person has blocked you.'
                friendset1 = Friendship.objects.filter(profile=self.request.user.profile, confirmed=True).only('friend')
                friendset2 = Friendship.objects.filter(profile=self.object, confirmed=True).only('friend')
                if friendset1.exists() and friendset2.exists():
                    commonfriends = list(filter(None,
                                                frozenset(friendset1.values_list('friend', flat=True)) &
                                                frozenset(friendset2.values_list('friend', flat=True))))
                    mutuals_count = len(commonfriends)
                    self.request.session['commonfriends'] = commonfriends
                context['mutuals_count'] = mutuals_count
        return context

class FriendshipView(SingleObjectMixin, FormView):
    form_class = FriendshipForm
    template_name ='simuser/show_user_profile.html'
    fields = [ 'confirmed' ]
    model = Profile

    def get_success_url(self):
        return reverse('profile-detail', kwargs={'pk': self.object.pk})

    def form_valid(self, form):
        self.object = self.get_object()
        confirmed = form.get_confirmed()
        friendship, created = Friendship.objects.get_or_create(profile=self.request.user.profile, friend=self.object.user)
        friendship.confirmed = confirmed
        friendship.save()
        return super(FriendshipView, self).form_valid(form)

class UpdateProfileView(UpdateView):
    model = Profile
    form_class =  modelform_factory(Profile, fields =( 'birthday', 'bio' ),
                                    widgets={"birthday": forms.SelectDateWidget(years=range(1950, 2016))})
    context_object_name = 'profile'
    template_name_suffix = '_update_form'
    success_url = reverse_lazy('index')

    def get_object(self, queryset=None):
        return self.request.user.profile

class IndexView(FormView):
    form_class = FriendshipForm
    fields = [ 'confirmed' ]
    template_name = 'simuser/index.html'

    def get_success_url(self):
        return reverse('index')

    def form_valid(self, form):
        if 'reversed_friend' in self.request.session:
            reversed_friend = User.objects.get(pk=self.request.session['reversed_friend'])
            friendship = Friendship(profile=self.request.user.profile, friend=reversed_friend)
            friendship.confirmed = form.get_confirmed()
            friendship.save()
            del self.request.session['reversed_friend']
        return super(IndexView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context["form"] = None
        if self.request.user.is_authenticated():
            for reversed_friendship in Friendship.objects.filter(friend=self.request.user, confirmed=True):
                try:
                    friendship = Friendship.objects.get(profile=self.request.user.profile, friend=reversed_friendship.profile.user)
                except Friendship.DoesNotExist:
                    self.initial = {'confirmed': None}
                    form = self.get_form()
                    context["form"] = form
                    context['message'] = '%s has sent you a friend request.' % reversed_friendship.profile.user
                    self.request.session['reversed_friend'] = reversed_friendship.profile.user.pk
                    break
                else:
                    continue
        return context

class CommonFriendsView(ListView):
    model = User
    context_object_name = 'users'
    template_name = 'simuser/users_list.html'

    def get_queryset(self):
        if 'commonfriends' in self.request.session:
            queryset = User.objects.filter(pk__in=self.request.session['commonfriends'])
            del self.request.session['commonfriends']
            return queryset
        return super(CommonfriendsView, self).get_queryset()
