from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db.models.signals import post_save
from django.utils.encoding import python_2_unicode_compatible

# Create your models here.

BOOL_CHOICES = ((True, 'Yes'), (False, 'No'))

@python_2_unicode_compatible
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    birthday = models.DateField(null=True, blank=True, verbose_name='Date of birth')
    bio = models.TextField(max_length=5000, blank=True, null=True, verbose_name='Biography')
    friends = models.ManyToManyField(User,
                                     through='Friendship',
                                     through_fields=('profile', 'friend'),
                                     symmetrical=False,
                                     related_name = '+',
                                     blank=True,
    )

    def __str__(self):
        return str(self.user) + "'s profile"

    def get_absolute_url(self):
        return reverse('profile-detail', kwargs={'pk': self.pk})

class Friendship(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True)
    friend = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    confirmed = models.BooleanField(choices=BOOL_CHOICES, default=False, verbose_name='Friends?')

    class Meta:
        unique_together = ( 'profile', 'friend' )


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

post_save.connect(create_user_profile, sender=User)
