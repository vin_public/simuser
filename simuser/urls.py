from . import views
from django.conf.urls import url

urlpatterns = [
    url(r'^users/$', views.UsersListView.as_view(), name='userslist'),
    url(r'^users/common/$', views.CommonFriendsView.as_view(), name='commonfriends'),
    url(r'^profiles/(?P<pk>[0-9]+)/$', views.ProfileFormView.as_view(), name='profile-detail'),
    url(r'^profiles/(?P<pk>[0-9]+)$', views.UpdateProfileView.as_view(), name='updateprofile'),
    url(r'^register/$', views.RegistrationView.as_view(), name='registration'),
    url(r'^index/$', views.IndexView.as_view(), {'template_name': 'simuser/index.html'}, name='index')
]
